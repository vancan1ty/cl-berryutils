;; basic examples of using PCL's micro unit testing framework (from PCL by Peter Siebel)
(defpackage :giga-unit-test
  (:use :common-lisp :giga-unit)
  (:export ))
(in-package :giga-unit-test)

(deftest test-+ ()
  (check
    (= (+ 1 2) 3)
    (= (+ 1 2 3) 6)
    (= (+ -1 -3) -4)))

(deftest test-* ()
  (check
    (= (* 2 2) 4)
    (= (* 2 2 3) 12)
    (= (* -1 -3) 3)))

(deftest test-arithmetic ()
  (combine-results
   (test-+)
   (test-*)))

(test-arithmetic)
