;; Unit tests library mostly identical to the one included in Practical Common Lisp by Peter Seibel

(defsystem "giga-unit"
   :description "unit testing facility, based on PCL by Peter Seibel"
   :version "0.1"
   :components ((:file "giga-unit")))
