;;; -*- mode: common-lisp; -*-

;;; Copyright (c) 2017, Currell Berry.  All rights reserved.
;;; License for all software in this package: simplified BSD license.

(defpackage #:berryutils-asd
  (:use :cl :asdf))

(in-package :berryutils-asd)

(defsystem berryutils 
  :name "berryutils"
  :version "0.5"
  :author "Currell Berry"
  :licence "simplified BSD License"
  :description "Some utility functions"
  :depends-on (:cl-ppcre :puri :ironclad :alexandria)
  :components ((:file "berryutils"))) 
