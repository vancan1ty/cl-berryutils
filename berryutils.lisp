(require :alexandria)
(require :cl-ppcre)
(require :puri)
(require :ironclad)
(require :closer-mop)
(defpackage :berryutils
  (:use :common-lisp :cl-ppcre :puri :alexandria)
  (:export :break-transparent
           :print-transparent
           :print-transparent-m
	   :range
	   :prompt-read
	   :zip-plist
	   :enumerate-hash-table
	   :slurp-stream4
	   :simplify-line-endings
	   :easy-uri-merge
	   :slurp-file
	   :list-to-array
	   :hash-password
           :get-elements
           :read-output-stream-to-string-nonblocking
           :lefun
           :setup-object-printing
           :random-in-range
           :random-alphanumeric-string
           :system-exit!
           :read-sequence-nonblock
           :->
           ))

(in-package :berryutils)

;;;////////////////////////////////UTILITY CODE
(defmacro break-transparent (exp)
  `(let ((x ,exp)) (break "argument to break: ~:S" x) x))

(defmacro print-transparent (exp)
  `(let ((tvar987 ,exp)) (format t "argument: ~a~%" tvar987) tvar987))

(defmacro print-transparent-m (message exp)
  `(let ((tvar987 ,exp)) (format t "p-t-m--~a: ~a~%" ,message tvar987) tvar987))

;(macroexpand (print-transparent "hello"))

(defun range (&key (min 0) (max 0) (step 1))
  "returns range from min to max inclusive of min
   exclusive of max"
   (loop for n from min below max by step
      collect n))

(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (read-line *query-io*))

(defun zip-plist (keys values)
  "creates a plist from a list of keys and a list of values."
  (loop for k in keys
        for v in values nconc
       (list k v)))

(defun enumerate-hash-table (hasht)
  (maphash #'(lambda (k v) (format t "~a => ~a~%" k v)) hasht))

(defun slurp-stream4 (stream)
 (let ((seq (make-string (file-length stream))))
  (read-sequence seq stream)
  seq))

(defun pprint-plist (plist)
    (format t "~{~a:~10t~a~%~}~%" plist))

(defun simplify-line-endings (text)
  "replaces all sorts of weird line endings with the standard cl line ending #\newline"
  (cl-ppcre:REGEX-REPLACE-ALL "(\\r|\\n)+" text (string #\newline)))

(defun easy-uri-merge (rooturl relpath)
  "merges the relpath onto the root url, returns the result as a string"
  (with-output-to-string (ostring) 
    (puri:render-uri (puri:merge-uris relpath rooturl) 
		     ostring)))

(defun slurp-file (filename)
  (with-open-file (stream filename)
    (slurp-stream4 stream)))

(defun contains (item list)
  (if (position item list)
      t
      nil))

(defun is-true-for-all (fn list)
  (loop for item in list do
       (if (not (funcall fn item))
	   (return-from is-true-for-all nil)))
  t)

;;;////////////////////////////////

;;;candidate functions...
(defun or-equal (i1 &rest others)
  "returns true if i1 equals one of items in others"
  (loop for oitem in others do
       (if (equal i1 oitem)
	   (return-from or-equal t)))
  nil)

(defun nth-largest (list n &key (key #'identity))
  (elt (sort (remove-duplicates list :key key) #'> :key key) (- n 1)))

(declaim (ftype (function (list) simple-array) list-to-array))

(defun list-to-array (list)
  (make-array (length list) :initial-contents list))


(defun hash-password (password)
  (ironclad:byte-array-to-hex-string 
   (ironclad:digest-sequence 
    :sha256 
    (ironclad:ascii-string-to-byte-array password))))

;;; ///////////////////////////////////////////

;;override print-object for hash tables
(defmethod print-object ((object hash-table) stream)
  (format stream "#HASH{~{~{(~a : ~a)~}~^ ~}}"
          (loop for key being the hash-keys of object
                using (hash-value value)
                collect (list key value))))

(defun get-elements (sequence indexes)
  (loop for index in indexes collect
       (elt sequence index)))

(defun read-output-stream-to-string-nonblocking (ostream)
  "reads as much data as ostream currently has, puts it all in to a string, and returns the string"
  (declare (optimize debug))
  (let ((obuffer (make-array 30 :element-type 'character :fill-pointer 0 :adjustable t))
        #|(count 0)|#)
    (declare (optimize debug))
    (loop while (open-stream-p ostream)  do
         (let ((char (read-char-no-hang ostream nil :eof)))
           (if (or (eql char nil) (eql char :eof)) ;then we are done reading in data for now
               (return-from read-output-stream-to-string-nonblocking (subseq obuffer 0 (length obuffer))))
           (vector-push-extend char obuffer)))))

;; the below code based on read-buf-nonblock from the sbcl contrib sb-bsd-sockets "tests.lisp"
;; note I know that this function has bug conditions, please do not use till it is fixed
(defun read-sequence-nonblock (seq stream &key (start 0) end)
  "Like READ-SEQUENCE, but returns early if the full quantity of data isn't there to be read.  Blocks if no input at all. "
  (when (eql end nil)
    (setf end (length seq)))
  (let ((eof (gensym)))
    (do ((i start (1+ i))
         (c (read-char stream nil eof)
            (read-char-no-hang stream nil eof)))
        ((or (>= i end)
             (not c)
             (eq c eof))
         i)
      (setf (elt seq i) c))))


(defun read-ascii-file-to-binary-array (filename)
  (with-open-stream (stream (open filename :element-type '(unsigned-byte 8)))
    (let* ((buffer
            (make-array (file-length stream)
                        :element-type
                        '(unsigned-byte 8))))
      (read-sequence buffer stream)
      buffer)))

(defmacro lefun (name parameters &body body)
  "Just like lefun but adds automatic logging of the parameters and the time it took to return."
  (when (typep (car body) 'string)
    (setf body (cdr body)))
  `(defun ,name ,parameters
     (format t "INFO: ~a called with parameters ~s~%" ',name (list ,@parameters))
     (let ()
       ,@body)))


;; (defun setup-object-printing ()
;;   (set-macro-character 
;;    #\{
;;    #'(lambda (str char)
;;        (declare (ignore char))
;;        (let ((list (read-delimited-list #\} str t)))
;;          (let ((type (first list))
;;                (list (second list)))
;;            (let ((class (allocate-instance (find-class type))))
;;              (loop for i in list do
;;                   (setf (slot-value class (car i)) (cdr i)))
;;              class)))))

;;   (defmethod print-object ((object standard-object) stream)
;;     (format stream "{ ~s ~s}" (type-of object)
;;             (loop for i in (mapcar #'closer-mop:slot-definition-name (closer-mop:class-slots (class-of object)))
;;                collect (cons i (slot-value object i)))))

;;   (defmethod print-object ((o asdf:sequential-plan) stream)
;;     (if *print-pretty*
;;         (pprint-logical-block (stream nil)
;;           (print-unreadable-object (o stream :type t :identity t)))
;;         (print-unreadable-object (o stream :type t :identity t))))
;;   )

;; idea taken from serapeum
(defun random-in-range (low high)
  (when (eql low high)
    (error 'arithmetic-error :operation 'random-in-range :operands (list low high)))
  (+ (min low high) (random (- (max low high) (min low high)))))

;; CB got code from the below link
;;http://www.codecodex.com/wiki/Generate_a_random_password_or_random_string#Common_Lisp
(defun random-alphanumeric-string (length)
  (let ((chars "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"))
    (coerce (loop repeat length collect (aref chars (random (length chars))))
            'string)))

;; code for this taken from the below link
;; http://www.cliki.net/Portable%20Exit
(defun system-exit! (&optional code)
      ;; This group from "clocc-port/ext.lisp"
      #+allegro (excl:exit code)
      #+clisp (#+lisp=cl ext:quit #-lisp=cl lisp:quit code)
      #+cmu (ext:quit code)
      #+cormanlisp (win32:exitprocess code)
      #+gcl (lisp:bye code)                     ; XXX Or is it LISP::QUIT?
      #+lispworks (lw:quit :status code)
      #+lucid (lcl:quit code)
      #+sbcl (sb-ext:exit :code code)
      ;; This group from Maxima
      #+kcl (lisp::bye)                         ; XXX Does this take an arg?
      #+scl (ext:quit code)                     ; XXX Pretty sure this *does*.
      #+(or openmcl mcl) (ccl::quit)
      #+abcl (cl-user::quit)
      #+ecl (si:quit)
      ;; This group from <hebi...@math.uni.wroc.pl>
      #+poplog (poplog::bye)                    ; XXX Does this take an arg?
      #-(or allegro clisp cmu cormanlisp gcl lispworks lucid sbcl
            kcl scl openmcl mcl abcl ecl)
      (error 'not-implemented :proc (list 'quit code))) 


;; -> access style
;; code written by Pascal Bourguignon, found on comp.lang.lisp
;; https://groups.google.com/forum/#!original/comp.lang.lisp/gnGCIasIrKo/o9wfrltLqxsJ

(defun length<= (length list)
  (or (zerop length) (and list (length<= (1- length) (cdr list)))))

(defun -> (object &rest path)
  (cond
    ((null path) object)
    ((arrayp object)
     (let ((d (array-rank object)))
       (if (length<= d path)
           (apply (function ->)
                  (apply (function aref) object (subseq path 0 d))
                  (nthcdr d path))
           (make-array                  ; let's get a slice
            (nthcdr (length path) (array-dimensions object))
            :displaced-to object
            :displaced-index-offset
            (apply (function array-row-major-index) object
                   (append path (make-list (- d (length path))
                                           :initial-element 0)))
            :element-type (array-element-type object)))))
    ((and (listp object)
          (every (function consp) object)
          (not (integerp (first path))))
     (apply (function ->) (cdr (assoc (first path) object)) (rest path)))
    ((listp object)
     (apply (function ->) (elt object (first path)) (rest path)))
    ((hash-table-p object)
     (apply (function ->) (gethash (first path) object) (rest path)))
    ((typep object '(or structure-object standard-object))
     (apply (function ->) (funcall (first path) object) (rest path)))
    ((or (functionp (first path))
         (and (symbolp (first path)) (fboundp (first path))))
     (apply (function ->) (funcall (first path) object) (rest path)))
    (t (error "This is not a compound object: ~S" object))))

;; below code taken from https://groups.google.com/forum/#!topic/comp.lang.lisp/uc9fLw7i0Fo
;; written by Pascal Bourguignon
(defun (setf ->) (value object &rest path)
  (if (null path)
      (error "(setf ->) needs some path.")
      (let ((last (car (last path))))
        (let ((object (apply (function ->) object (butlast path))))
          (cond
            ((arrayp object) (setf (aref object last) value))
            ((and (listp object)
                  (every (function consp) object)
                  (not (integerp last)))
             (setf (cdr (assoc last object)) value))
            ((listp object) (setf (elt object last) value))
            ((hash-table-p object) (setf (gethash last object) value))
            ((typep object '(or structure-object standard-object))
             (eval `(setf (,last ',object) ',value)))
            ((or (functionp (first path))
                 (and (symbolp (first path)) (fboundp (first path))))
             (eval `(setf (,last ',object) ',value)))
            (t (error "This is not a compound object: ~S" object)))))))
